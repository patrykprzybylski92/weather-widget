var cities = ['Lodz', 'Warsaw', 'Berlin', 'New York', 'London'];
var numberOfCities = cities.length;
var numberOfDraws = 3;


$(document).ready(function()
{
	setInterval(function startDrawingCities()
	{
		shuffleArray(cities);
		return startDrawingCities;
	}(), 60*1000);
});


function shuffleArray(array) 
{
	for (let i = numberOfCities - 1; i > numberOfCities - numberOfDraws - 1; i--) 
    {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];

        setInterval(function getWeatherAndRefreshData()
        {
        	getInfoAboutTheWeather(array[i], i);
        	return getWeatherAndRefreshData;
        }(), 10*1000);
    }
}


function getInfoAboutTheWeather(city, index)
{
	var searchtext = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + city + "') and u='c'";

	 $.getJSON("https://query.yahooapis.com/v1/public/yql?q=" + searchtext + "&format=json", function(data)
	 {
	 	appendInfoAboutTheWeather(data, index);
	 }); 
}


function appendInfoAboutTheWeather(data, index)
{
	if (data && index)
	{
		$('.loader').hide();
		$('#widget').show();
		$('.city-' + index).html(data.query.results.channel.location.city);
		$('.temp-' + index).html(data.query.results.channel.item.condition.temp + "°" + data.query.results.channel.units.temperature);
		$('.text-info-' + index).html(data.query.results.channel.item.condition.text);
		var url = data.query.results.channel.item.link;
		$('.link-' + index).attr('href', url.substr(url.indexOf('*') + 1));
	}
	else
	{
		$('.loader').show();
	}
}